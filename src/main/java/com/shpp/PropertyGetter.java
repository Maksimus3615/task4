package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyGetter {
    static String message;

    private PropertyGetter() {
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyGetter.class);
    public static Properties getProperties(String path) {

        Properties properties = new Properties();
        InputStream in = PropertyGetter.class.getClassLoader().getResourceAsStream(path);
        try {
            if (in != null) {
                InputStreamReader inReader = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader buff = new BufferedReader(inReader);
                properties.load(buff);
            } else {
                throw new FileNotFoundException("file properties was not found...");
            }
        } catch (IOException e) {
            message = e.getMessage();
            LOGGER.error(message);
        }
        return properties;
    }
}