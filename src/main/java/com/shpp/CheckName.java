package com.shpp;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class CheckName implements
        ConstraintValidator<CheckNameSize, String> {
    @Override
    public void initialize(CheckNameSize productName) {
        // empty...
    }
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return (s.length()>4 && s.length()<16);
    }
}
