package com.shpp;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class TableMaker {
    private static final int LENGTH_NAME_MIN = 5;
    private static final int LENGTH_NAME_MAX = 15;
    private static final int NOT_LETTER_SYMBOL_MIN = 91;
    private static final int NOT_LETTER_SYMBOL_MAX = 96;
    private static final int NUMBER_OF_LETTER_MIN = 65;
    private static final int NUMBER_OF_LETTER_MAX = 123;
    private static final int QUANTITY = 3000000;
    private static final Random random = new Random();
    private static final Logger LOGGER = LoggerFactory.getLogger(TableMaker.class);
    private int productQuantity;

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    AtomicInteger idProduct = new AtomicInteger();

    public TableMaker() {
        productQuantity = QUANTITY;
    }

    public void cleanTables(Statement statement) throws SQLException {
        String sqlRequest =
                "TRUNCATE shop CASCADE ;\n" +
                        "TRUNCATE product CASCADE ;\n" +
                        "TRUNCATE stock CASCADE ;\n";

        statement.executeUpdate(sqlRequest);
        LOGGER.info("All tables are cleaned...");
    }

    public void createStockFiles() {
        File dataFile = new File("stock.csv");
        try (FileWriter writer = new FileWriter(dataFile);
             BufferedWriter bw = new BufferedWriter(writer)) {
            int idStock = 1;
            for (int i = 1; i <= productQuantity; i++) {
                int shop = random.nextInt(5) + 1;
                for (int j = 0; j < 3; j++) {
                    int quantity = random.nextInt(500);
                    bw.write("" + idStock + ";" + shop + ";" + i + ";" + quantity);
                    bw.newLine();
                    idStock++;
                    shop++;
                }
                if (idStock > 100000) break;
            }
            writer.flush();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void createProductFiles() {
        File dataFile = new File("product.csv");
        try (FileWriter writer = new FileWriter(dataFile);
             BufferedWriter bw = new BufferedWriter(writer)) {

            ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
            Validator validator = validatorFactory.getValidator();

            idProduct.set(1);
            Stream.generate(() -> new ProductPojo(idProduct.get(), getRandomName(getRandomLength())))
                    .limit(productQuantity).filter(x -> validator.validate(x).isEmpty()).
                    forEach(i -> {
                        try {
                            idProduct.getAndIncrement();
                            bw.write("" + i.getProductId() + ";" + i.getProductName());
                            bw.newLine();
                        } catch (IOException e) {
                            LOGGER.error(e.getMessage());
                        }
                    });
            productQuantity = idProduct.get()-1;
            writer.flush();
            validatorFactory.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void createShopFiles() {
        File dataFile = new File("shop.csv");
        String[] address = {"1;Kyiv, Sobornaya 122",
                "2;Lviv, Konovalcya 154",
                "3;Kropyvnytskyi, Popova 8",
                "4;Kramatorsk, M.Pryimachenko 14",
                "5;Sumy, Drujby 56'",
                "6;Odesa, Paustovskogo 14",
                "7;Dnipro, Svobody 26"};
        try (FileWriter writer = new FileWriter(dataFile);
             BufferedWriter bw = new BufferedWriter(writer)) {
            for (String s : address) {
                bw.write(s);
                bw.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private int getRandomLength() {
        return random.nextInt(LENGTH_NAME_MAX - LENGTH_NAME_MIN) + LENGTH_NAME_MIN;
    }

    private String getRandomName(int length) {
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int charNum = random.nextInt(NUMBER_OF_LETTER_MAX - NUMBER_OF_LETTER_MIN) + NUMBER_OF_LETTER_MIN;
            if (charNum < NOT_LETTER_SYMBOL_MIN || charNum > NOT_LETTER_SYMBOL_MAX) {
                name.append((char) (charNum));
            }
        }
        return name.toString();
    }

    public void generateTable(Connection connection, String tableName) throws SQLException {
        try {
            new CopyManager((BaseConnection) connection).copyIn(
                    "COPY " + tableName + " FROM STDIN with (FORMAT csv, DELIMITER';')",
                    new BufferedReader(new FileReader(getFilePath(tableName))));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public String getFilePath(String tableName) {
        return new File("").
                getAbsolutePath()
                + "/" + tableName + ".csv";
    }
}
// statement.executeUpdate("COPY " + tableName + " from '" + getFilePath(tableName) + "' DELIMITER ';'");
/*
public String getFilePath(String tableName) {
    return new File("").
            getAbsolutePath().replace("\\", "\\\\")
            + "\\\\" + tableName + ".csv";
}

 */