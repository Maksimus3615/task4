package com.shpp;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Constraint(validatedBy = CheckName.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckNameSize {
    String message() default "name size is not valid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
