package com.shpp;

import jakarta.validation.constraints.NotNull;

public class ProductPojo {
    @NotNull
    private final int productId;
    //   @Size(min = 5, message = "the name must contain at least 7 letters")
    //   @Size(max = 15, message = "the name must contain no more than 15 letters")
    @NotNull
    @CheckNameSize(message = "the name must contain at least 7 letters and no more than 15 letters")
    private final String productName;

    ProductPojo(int id, String name) {
        this.productId = id;
        this.productName = name;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }
}
