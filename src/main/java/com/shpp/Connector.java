package com.shpp;

import java.util.Properties;

public class Connector {
    private final String url;
    private final String user;
    private final String password;
    public Connector(String path) {
        Properties properties = PropertyGetter.getProperties(path);
        this.url = properties.getProperty("url");
        this.user = properties.getProperty("user");
        this.password = properties.getProperty("password");
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
