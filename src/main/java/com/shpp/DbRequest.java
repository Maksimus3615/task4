package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DbRequest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DbRequest.class);

    public String createSqlRequest(String idProduct) {
        String sqlRequestMaxQuantity =
                "(SELECT MAX(quantity) FROM stock" + " WHERE id_product = " + idProduct + ")";
        String sqlRequestIdSop =
                "(SELECT id_shop FROM stock WHERE " +
                        "(quantity = " + sqlRequestMaxQuantity + " AND id_product = " + idProduct + "))";
        return "SELECT address FROM shop WHERE id_shop = ANY " + sqlRequestIdSop;
    }

    public String createSqlResult(ResultSet resultSet) {
        String result = "No results were received...";
        try {
            StringBuilder stringBuilder = new StringBuilder();
            int iterator = 0;
            while (resultSet.next()) {
                stringBuilder.append(resultSet.getString("address"));
                stringBuilder.append("\n\t\t");
                iterator++;
            }
            if (iterator > 0)
                result = "The following matching results were found:\n\t\t" + stringBuilder;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            result = "No results were received because of exception...";
        }
        return result;
    }
    public String getProductId(String[] args) {
        String productId = "1";
        if (args.length != 0)
            productId = args[0];
        else
            LOGGER.warn("Default productId set to 1...");
        return productId;
    }
}
