package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.sql.*;

public class DbExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DbExplorer.class);

    public static void main(String[] args) {
        Connector connector = new Connector("db.properties");
        TableMaker table = new TableMaker();
        DbRequest request = new DbRequest();
        try (Connection connection = DriverManager.getConnection(
                connector.getUrl(),
                connector.getUser(),
                connector.getPassword());
             Statement statement = connection.createStatement()) {

            makeNewSession(statement, table, request, args);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void makeNewSession(Statement statement,
                                      TableMaker table,
                                      DbRequest request,
                                      String[] args) throws SQLException {

        StopWatch timer = new StopWatch();

        timer.start("tables cleaning");
        table.cleanTables(statement);
        timer.stop();

        timer.start("new data generating");
        createFiles(table);
        timer.stop();

        timer.start("tables writing");
        writeTables(table, statement);
        timer.stop();

        timer.start("request getting");
        createRequest(statement, request, args);
        timer.stop();

        String mess = timer.prettyPrint();
        LOGGER.info(mess);
    }

    public static void createRequest(Statement statement,
                                     DbRequest request,
                                     String[] args) throws SQLException {
        String sqlRequest = request.createSqlRequest(request.getProductId(args));
        String mess = request.createSqlResult(statement.executeQuery(sqlRequest));
        LOGGER.info(mess);
    }

    public static void writeTables(TableMaker table, Statement statement) throws SQLException {
        StopWatch timer = new StopWatch();

        timer.start("Generating of shop table");
        table.generateTable(statement.getConnection(), "shop");
        timer.stop();
        LOGGER.info("Generating of shop table is completed...");

        timer.start("Generating of product table");
        table.generateTable(statement.getConnection(), "product");
        timer.stop();
        String mess = "Generating of product table is completed...(" + table.getProductQuantity() + ")";
        LOGGER.info(mess);

        timer.start("Generating of stock table");
        table.generateTable(statement.getConnection(), "stock");
        timer.stop();
        LOGGER.info("Generating of stock table is completed...");
        LOGGER.info("All tables are completed...");

        mess = timer.prettyPrint();
        LOGGER.info(mess);
    }

    public static void createFiles(TableMaker table) {
        table.createProductFiles();
        table.createShopFiles();
        table.createStockFiles();
        LOGGER.info("All data files are completed...");
    }
}
