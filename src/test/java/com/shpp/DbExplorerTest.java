package com.shpp;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.sql.Statement;

import static org.mockito.Mockito.verify;

class DbExplorerTest {

    @Test
    void testChecksUsingMethodsInSession() {
        TableMaker table = Mockito.mock(TableMaker.class);
        DbRequest request = Mockito.mock(DbRequest.class);
        Statement statement = Mockito.mock(Statement.class);
        String[] args = {"2"};
        try {
            DbExplorer.makeNewSession(statement, table, request, args);
            verify(table).cleanTables(statement);
            verify(table).createProductFiles();
            verify(table).createShopFiles();
            verify(table).createStockFiles();
            verify(table).generateTable(statement.getConnection(), "shop");
            verify(table).generateTable(statement.getConnection(), "product");
            verify(table).generateTable(statement.getConnection(), "stock");
            verify(request).getProductId(args);
            verify(request).createSqlRequest(request.getProductId(args));
            verify(request).createSqlResult(
                    statement.executeQuery(
                            request.createSqlRequest(
                                    request.getProductId(args))));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}