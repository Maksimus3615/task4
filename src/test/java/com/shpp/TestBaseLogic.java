package com.shpp;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class TestBaseLogic {
    @Test
    void testPropertyGetterClass() {
        Connector connector = new Connector("test.properties");

        assertEquals("123", connector.getUrl());
        assertEquals("456", connector.getUser());
        assertEquals("789", connector.getPassword());
    }

    @Test
    void testCreatesSqlRequest() {
        DbRequest request = new DbRequest();
        String sqlRequest = request.createSqlRequest("2");
        String original = "SELECT address FROM shop WHERE id_shop = " +
                "ANY (SELECT id_shop FROM stock WHERE (quantity = " +
                "(SELECT MAX(quantity) FROM stock WHERE id_product = 2)" +
                " AND id_product = 2))";

        assertEquals(sqlRequest, original);
    }

    @Test
    void testImitatesGettingResultToRequest() throws SQLException {
        DbRequest request = new DbRequest();
        ResultSet testResultSet = Mockito.mock(ResultSet.class);
        when(testResultSet.next()).thenReturn(true, false);
        when(testResultSet.getString(anyString())).thenReturn("testAddress");
        String result = request.createSqlResult(testResultSet);

        assertEquals("The following matching results were found:\n\t\t"
                + "testAddress\n\t\t", result);
    }

    @Test
    void testImitatesGettingNoResultToRequest() {
        DbRequest request = new DbRequest();
        ResultSet testResultSet = Mockito.mock(ResultSet.class);
        String result = request.createSqlResult(testResultSet);

        assertEquals("No results were received...", result);
    }

    @Test
    void testImitatesGettingNoResultToRequestWithException() throws SQLException {
        DbRequest request = new DbRequest();
        ResultSet testResultSet = Mockito.mock(ResultSet.class);
        when(testResultSet.next()).thenThrow(new SQLException());
        String result = request.createSqlResult(testResultSet);

        assertEquals("No results were received because of exception...", result);
    }

    @Test
    void testGetsPropertiesFromNoFile() {
        PropertyGetter.getProperties("noName.file");
        assertEquals("file properties was not found...", PropertyGetter.message);
    }

    @Test
    void testGetsInvalidProductId() {
        String[] args = new String[0];
        DbRequest request = new DbRequest();
        String productId = request.getProductId(args);
        assertEquals("1", productId);
    }

    @Test
    void testGetsValidProductId() {
        String[] args = new String[1];
        args[0] = "2";
        DbRequest request = new DbRequest();
        String productId = request.getProductId(args);
        assertEquals("2", productId);
    }

    @Test
    void testCreatesProductCsvFile() {
        File file = new File("product.csv");
        delFile(file);
        TableMaker table = new TableMaker();
        table.setProductQuantity(1000);
        assertEquals(1000, table.getProductQuantity());
        table.createProductFiles();
        assertTrue(file.isFile());
        assertTrue(table.getProductQuantity() < 1000);
        delFile(file);
    }

    @Test
    void testCreatesShopCsvFile() {
        File file = new File("shop.csv");
        delFile(file);
        TableMaker table = new TableMaker();
        table.createShopFiles();
        assertTrue(file.isFile());
        delFile(file);
    }

    @Test
    void testCreatesStockCsvFile() {
        File file = new File("stock.csv");
        delFile(file);
        TableMaker table = new TableMaker();
        table.createStockFiles();
        assertTrue(file.isFile());
        delFile(file);
    }

    private void delFile(File file) {
        if (file.isFile()) {
            try {
                Files.delete(Path.of(file.getPath()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    void testGetsFilePath() {
        String expected = new File("").getAbsolutePath() + "/" + "table.csv";
        TableMaker table = new TableMaker();
        String original = table.getFilePath("table");
        assertEquals(expected, original);
    }
}
